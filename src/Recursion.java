public class Recursion {

    public static void main (String args[]){
        // Using Recursion execute the FizzBuzz exercise

        recFB(100);

    }


    public static int recFB(int i){
        if (i >=100) {
            System.out.println(i);
            return i;
        }

        else if (i % 3 == 0 && i % 5 != 0){
            System.out.println("Fizz");
        }
        else if (i % 5 == 0 && i % 3 != 0){
            System.out.println("Buzz");
        }
        else if (i % 3 == 0 && i % 5 == 0){
            System.out.println("FizzBuzz");
        }
        else
            System.out.println(i);
        return recFB(i+1);


    }
}





